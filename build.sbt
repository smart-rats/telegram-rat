ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "org.zella"
ThisBuild / organizationName := "zella"

lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := "telegram-rat",
  )
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "org.zella.tgrat"
  )

assemblyOutputPath in assembly := file("build/assembly.jar")

// META-INF discarding
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case PathList(ps@_*) if ps.last endsWith ".conf" => MergeStrategy.concat
  case x => MergeStrategy.last
}

test in assembly := {}

mainClass in assembly := Some("org.zella.tgrat.Runner")

// Core with minimal dependencies, enough to spawn your first bot.
libraryDependencies += "com.bot4s" %% "telegram-core" % "4.0.0-RC2"

// Extra goodies: Webhooks, support for games, bindings for actors.
//libraryDependencies += "com.bot4s" %% "telegram-akka" % "4.0.0-RC2"
// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
// https://mvnrepository.com/artifact/org.typelevel/cats-core
//libraryDependencies += "org.typelevel" %% "cats-core" % "1.6.0"
// https://mvnrepository.com/artifact/com.softwaremill.sttp/monix
libraryDependencies += "com.softwaremill.sttp" %% "monix" % "1.5.12"
// https://mvnrepository.com/artifact/io.monix/monix-eval
libraryDependencies += "io.monix" %% "monix-eval" % "3.0.0-RC2"
// https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.+"

libraryDependencies += "com.softwaremill.sttp" %% "core" % "1.5.12"
// https://mvnrepository.com/artifact/com.softwaremill.sttp/okhttp-backend-monix
libraryDependencies += "com.softwaremill.sttp" %% "okhttp-backend-monix" % "1.5.12"
// https://mvnrepository.com/artifact/com.typesafe.play/play-json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.7.3"

libraryDependencies += "ai.x" %% "play-json-extensions" % "0.30.1"

// https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

libraryDependencies += "com.typesafe" % "config" % "1.3.4"

libraryDependencies += "io.vertx" %% "vertx-lang-scala-stack" % "3.6.2" % "provided" pomOnly()
// https://mvnrepository.com/artifact/io.vertx/vertx-web-scala
libraryDependencies += "io.vertx" %% "vertx-web-scala" % "3.6.2"

libraryDependencies += "com.github.zella" %% "rx-process2" % "0.1.0-BETA"

libraryDependencies += "com.github.davidmoten" % "rxjava2-extras" % "0.1.+"

libraryDependencies += "io.monix" %% "monix-reactive" % "3.0.0-RC2"

libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.7.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test