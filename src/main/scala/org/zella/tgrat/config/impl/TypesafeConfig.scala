package org.zella.tgrat.config.impl

import java.nio.file.{Files, Path, Paths}

import better.files.{File, Resource}
import com.softwaremill.sttp.{Uri, _}
import com.typesafe.config.Config
import org.zella.tgrat.config.{GassistantEmbeddedSpeech, TgRatConfig, SocksProxy}
import org.zella.tgrat.model.CommandDef
import play.api.libs.json.Json

import scala.concurrent.duration.{FiniteDuration, _}
import scala.util.Try

class TypesafeConfig(conf: Config) extends TgRatConfig {

  override val commands: Set[CommandDef] =
    Json.parse(Try(conf.getString("commands.defs"))
      .getOrElse(File(conf.getString("commands.defsFile")).contentAsString)).as[Set[CommandDef]]


  override val baseSearchTime: FiniteDuration = conf.getInt("search.timeSec").seconds

  override val playTimeout: FiniteDuration = conf.getInt("play.timeoutSec").seconds

  override val searchPageSize: Int = conf.getInt("search.pageSize")

  override val socksProxy: Option[SocksProxy] = Try(SocksProxy(
    conf.getString("proxy.host"),
    conf.getInt("proxy.port"),
    Try(conf.getString("proxy.user")).toOption,
    Try(conf.getString("proxy.pass")).toOption
  )).toOption

  override val botToken: String = conf.getString("bot.token")

  override val smartRatUrl: Uri = uri"${conf.getString("smartRat.url")}"

  override val playOn: String = conf.getString("smartRat.playOn")

  override val httpPort: Int = conf.getInt("endpoint.port")

  override val gassistantEmbeddedSpeech: Option[GassistantEmbeddedSpeech] = {
    Try(GassistantEmbeddedSpeech(Paths.get(Try(conf.getString("script.hotword"))
      .getOrElse(extractResource("hotword.py"))),
      Paths.get(Try(conf.getString("script.recognize"))
        .getOrElse(extractResource("recognize.py"))),
      conf.getString("assistant.deviceId"),
      Paths.get(conf.getString("assistant.credentials")),
      conf.getString("recognizer.key"),
      Paths.get(Try(conf.getString("script.gtts"))
        .getOrElse(extractResource("gtts.py"))),
      conf.getString("gtts.lang")
    )).toOption
  }

  private def extractResource(name: String) = {
    val f = Paths.get(name)
    if (!Files.exists(f))
      Files.copy(Resource.getAsStream(name), f)
    f.toAbsolutePath.toString
  }

}
