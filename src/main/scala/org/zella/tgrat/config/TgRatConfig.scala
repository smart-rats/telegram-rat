package org.zella.tgrat.config

import java.nio.file.Path

import com.softwaremill.sttp.Uri
import org.zella.tgrat.model.CommandDef

import scala.concurrent.duration.FiniteDuration

trait TgRatConfig {
  def commands: Set[CommandDef]

  def baseSearchTime: FiniteDuration

  def playTimeout: FiniteDuration

  def searchPageSize: Int

  def socksProxy: Option[SocksProxy]

  def botToken: String

  def smartRatUrl: Uri

  def playOn: String

  def httpPort: Int

  def gassistantEmbeddedSpeech: Option[GassistantEmbeddedSpeech]

}

case class SocksProxy(host: String, port: Int, user: Option[String], pass: Option[String])

case class GassistantEmbeddedSpeech(scriptHotWord: Path,
                                    scriptRecognize: Path,
                                    assistantDeviceId: String,
                                    assistantCredentials: Path,
                                    speechRecognizeKey: String,
                                    scriptGtts:Path,
                                    gttsLang:String)
