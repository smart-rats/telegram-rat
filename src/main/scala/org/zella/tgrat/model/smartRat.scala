package org.zella.tgrat.model

import play.api.libs.json.{Format, Json}

case class SearchInput(mediaType: String,
                       text: String,
                       maxSearchTimeSec: Int,
                       pageSize: Int,
                       searchExcept: Seq[SearchExcept])

object SearchInput {
  implicit val format: Format[SearchInput] = Json.format[SearchInput]
}

case class SearchExcept(hash: String,
                        indexes: Set[Int])

object SearchExcept {
  implicit val format: Format[SearchExcept] = Json.format[SearchExcept]
}

case class PlayInput(hash: String, index: Int, streaming: String)

object PlayInput {
  implicit val format: Format[PlayInput] = Json.format[PlayInput]
}

case class SearchAndPlayInput(mediaType: String,
                              text: String,
                              streaming: String,
                              maxSearchTimeSec: Int)

object SearchAndPlayInput {
  implicit val format: Format[SearchAndPlayInput] = Json.format[SearchAndPlayInput]
}

case class FilteredItem(file: TorrentFileDesc, score: Float, index: Int, torrentMeta: TorrentMeta)

object FilteredItem {
  implicit val format: Format[FilteredItem] = Json.format[FilteredItem]
}

case class TorrentMeta(seeders: Int)

object TorrentMeta {
  implicit val format: Format[TorrentMeta] = Json.format[TorrentMeta]
}

case class TorrentFileDesc(index: Int, paths: Seq[String], size: Long, hash: String) {
  def mkString = paths.mkString("/")
}

object TorrentFileDesc {
  implicit val format: Format[TorrentFileDesc] = Json.format[TorrentFileDesc]
}


case class SearchOutput(items: Seq[FilteredItem])

object SearchOutput {
  implicit val format: Format[SearchOutput] = Json.format[SearchOutput]
}


