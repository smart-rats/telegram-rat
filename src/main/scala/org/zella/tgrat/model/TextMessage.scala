package org.zella.tgrat.model

import org.zella.tgrat.model.Interaction.Interaction

trait User {
  def id: String
  def interaction: Interaction
}

case class TelegramUser(chatId: String) extends User {
  override def id: String = chatId
  override def interaction: Interaction = Interaction.Telegram
}

case class SpeechUser(userId: String) extends User {
  override def id: String = userId
  override def interaction: Interaction = Interaction.Speech
}

case class TextMessage(text: String, user: User)
