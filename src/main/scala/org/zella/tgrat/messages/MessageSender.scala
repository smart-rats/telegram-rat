package org.zella.tgrat.messages

import org.zella.tgrat.model.{FilteredItem, SearchInput, User}

trait MessageSender {

  def sendSearchOutput(searchInput: SearchInput, items: Map[Int, FilteredItem], user: User)

  def sendText(text: String, user: User)

}
