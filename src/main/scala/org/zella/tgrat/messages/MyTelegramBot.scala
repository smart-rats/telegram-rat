package org.zella.tgrat.messages

import java.net.{Authenticator, InetSocketAddress, PasswordAuthentication, Proxy}

import akka.actor.ActorRef
import com.bot4s.telegram.api.declarative.Commands
import com.bot4s.telegram.api.{Polling, RequestHandler, TelegramBot}
import com.bot4s.telegram.clients.ScalajHttpClient
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import org.zella.tgrat.config.TgRatConfig
import org.zella.tgrat.model.{TelegramUser, TextMessage}

class MyTelegramBot(conf: TgRatConfig) extends TelegramBot with Commands with Polling {


  //  val proxy = new Proxy(Proxy.Type.SOCKS, InetSocketAddress.createUnresolved("35.190.184.200", 1080))

  private val proxy = conf.socksProxy.map(p => {

    def getAuth(user: String, password: String): Authenticator = new Authenticator() {
      override def getPasswordAuthentication = new PasswordAuthentication(user, password.toCharArray)
    }

    //
    if (p.user.isDefined && p.pass.isDefined) {
      Authenticator.setDefault(getAuth(p.user.get, p.pass.get))
    }

    new Proxy(Proxy.Type.SOCKS, InetSocketAddress.createUnresolved(p.host, p.port))
  }).getOrElse(Proxy.NO_PROXY)


  override val client: RequestHandler = new ScalajHttpClient(conf.botToken, proxy)

  val log: Logger =
    Logger(LoggerFactory.getLogger(getClass.getName))
  var source: Option[ActorRef] = None

  def setSource(source: ActorRef): Unit = this.source = Some(source)

  onMessage { implicit m =>
    log.debug("receive telegram message")
    //TODO can be checked compile time with phantom types
    if (source.isEmpty) {
      throw new IllegalStateException("You forget to set source actor")
    }
    //only text messages support
    m.text.foreach(text => source.foreach(s => s ! TextMessage(text, TelegramUser(m.source.toString))))
  }
}