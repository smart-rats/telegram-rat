package org.zella.tgrat.messages.impl

import com.bot4s.telegram.api.BotBase
import com.bot4s.telegram.methods.{ParseMode, SendMessage}
import com.bot4s.telegram.models.{KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove}
import org.zella.tgrat.messages.MessageSender
import org.zella.tgrat.model.{FilteredItem, SearchInput, User}

class TelegramMessageSender(bot: BotBase) extends MessageSender {

  override def sendSearchOutput(searchInput: SearchInput, items: Map[Int, FilteredItem],user: User): Unit = {
    //TODO code smells bad
    bot.request(SendMessage(chatId = user.id,
      text = (Seq(searchInput.text) ++ items.toIndexedSeq.sortBy(_._1).map { case (i, f) => s"$i. ${f.file.mkString} [seed ${f.torrentMeta.seeders}]" })
        .mkString(System.lineSeparator()),
      replyMarkup = Some(ReplyKeyboardMarkup.singleColumn(
        items.keys.toIndexedSeq.sorted.map(i => KeyboardButton.text(i.toString)).toSeq :+ KeyboardButton.text("Дальше")
      ))))
  }

  override def sendText(text: String, user: User): Unit = {
    bot.request(SendMessage(chatId = user.id, text = text, replyMarkup = Some(ReplyKeyboardRemove(removeKeyboard = true))))
  }

}
