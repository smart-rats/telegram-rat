package org.zella.tgrat.messages.impl

import com.softwaremill.sttp.Uri
import org.zella.tgrat.config.{GassistantEmbeddedSpeech, TgRatConfig}
import org.zella.tgrat.messages.MessageSender
import org.zella.tgrat.model.{FilteredItem, SearchInput, User}
import org.zella.tgrat.utils.Subprocess

class GttsMessageSender(gassistConf: GassistantEmbeddedSpeech) extends MessageSender {

  //TODO not typesafe, should be in interaction
  override def sendSearchOutput(searchInput: SearchInput, items: Map[Int, FilteredItem], user: User): Unit =
    throw new UnsupportedOperationException("Not supported in speech mode")


  override def sendText(text: String, user: User): Unit = {
    Subprocess.playGtts(gassistConf.scriptGtts, text, gassistConf.gttsLang)
      .subscribe()
  }

}
