package org.zella.tgrat.utils

import monix.reactive.Observable

import scala.concurrent.duration.FiniteDuration

object Utils {

  def retryWithDelay[A](source: Observable[A], delay: FiniteDuration): Observable[A] =
    source.onErrorHandleWith { _ =>
      retryWithDelay(source, delay).delayExecution(delay)
    }
}
