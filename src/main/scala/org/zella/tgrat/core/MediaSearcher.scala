package org.zella.tgrat.core

import java.util.concurrent.TimeoutException

import akka.actor.Actor
import akka.pattern.pipe
import com.softwaremill.sttp.okhttp.monix.OkHttpMonixBackend
import com.softwaremill.sttp.{asString, sttp, _}
import com.typesafe.scalalogging.LazyLogging
import monix.execution.Scheduler.Implicits.global
import org.zella.tgrat.config.TgRatConfig
import org.zella.tgrat.messages.MessageSender
import org.zella.tgrat.model.Interaction.Interaction
import org.zella.tgrat.model.{FilteredItem, PlayInput, SearchInput, SearchOutput, _}
import org.zella.tgrat.utils.JUtils
import play.api.libs.json.Json

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.util.Try


case class FallbackToUser(reason: Throwable, user: User) extends RuntimeException(reason)

class MediaSearcher(senders: Map[Interaction, MessageSender], conf: TgRatConfig) extends Actor with LazyLogging {

  implicit val httpBackend = OkHttpMonixBackend()

  def internalErrorHandle: Receive = {
    case e: Exception =>
      logger.error("Error here:", e)
      context.become(waitCommand)
  }

  def toUserErrorHandle: Receive = {
    case e: Exception =>
      logger.error("Error here:", e)
      e match {
        case t: FallbackToUser =>
          senders(t.user.interaction).sendText(t.reason match {
            case e: TimeoutException => "Не удалось, прости"
            case _ => "Ошибка случилась"
          }, t.user)
        case _ =>
      }
      context.become(waitCommand)
  }

  private def doSearch(input: SearchInput, user: User) = {
    senders(user.interaction).sendText("Поняла, ищу ...", user) //TODO conf
    sttp
      .body(Json.toJson(input).toString())
      .post(uri"${conf.smartRatUrl}/api/v1/search")
      .response(asString)
      .send.map(s => Json.parse(s.unsafeBody).as[SearchOutput])
      .timeout(input.maxSearchTimeSec.seconds * 2) //not necessary
      .redeem(e => FallbackToUser(e, user), identity)
      .runToFuture
      .pipeTo(self)
  }

  private def doSearchAndPlay(input: SearchAndPlayInput, user: User) = {
    senders(user.interaction).sendText("Поняла, ищу ...", user) //TODO conf
    sttp
      .body(Json.toJson(input).toString())
      .post(uri"${conf.smartRatUrl}/api/v1/searchAndPlay")
      .response(asString)
      .send.map(_.unsafeBody)
      .timeout(conf.playTimeout)
      .redeem(e => FallbackToUser(e, user), identity)
      .runToFuture
      .pipeTo(self)
  }


  def waitCommand: Receive = internalErrorHandle orElse {
    case m: TextMessage =>
      //TODO rework with pattern matching
      Try {
        conf.commands
          .filter(c => c.support.contains(m.user.interaction))
          .map(c => c.matches(JUtils.wordsLowerCase(m.text).asScala))
          .find(_.isDefined).flatten.get //throws err and goes to "Не поняла, бро"
      }.flatMap(params => Try[Unit] {
        if (params.keySet == Set("find", "media", "subject")) {
          val search = SearchInput(params("media"), params("subject"), conf.baseSearchTime.toSeconds.toInt, conf.searchPageSize, Seq())
          context.become(waitSearchResult(search, m.user))
          doSearch(search, m.user)
        } else if (params.keySet == Set("play", "media", "subject")) {
          val searchAndPlay = SearchAndPlayInput(params("media"), params("subject"), conf.playOn, conf.baseSearchTime.toSeconds.toInt)
          context.become(waitPlayResult(m.user))
          doSearchAndPlay(searchAndPlay, m.user)
        } else if (params.keySet == Set("find", "subject")) {
          val search = SearchInput("any", params("subject"), conf.baseSearchTime.toSeconds.toInt, conf.searchPageSize, Seq())
          context.become(waitSearchResult(search, m.user))
          doSearch(search, m.user)
        }
        else throw new Exception("Command not supported: " + params.toString())
      }).getOrElse({
        senders(m.user.interaction).sendText("Не поняла, бро", m.user)
        context.become(waitCommand)
      })
  }


  def waitSearchResult(search: SearchInput, user: User): Receive = toUserErrorHandle orElse {
    case m: TextMessage =>
      senders(m.user.interaction).sendText("Ну хорошо, прерываю поиск. Жду новой команды", user)
      context.become(waitCommand)
    case searchOut: SearchOutput =>
      val items = (Stream from 1).zip(searchOut.items).toMap
      context.become(waitSelection(items, search))
      senders(user.interaction).sendSearchOutput(search, items, user)
  }

  def waitSelection(items: Map[Int, FilteredItem], search: SearchInput): Receive = internalErrorHandle orElse {
    case m: TextMessage if m.text == "Дальше" => //TODO conf
      val searchExceptOld = search.searchExcept
      //Not optimized, but page size small =)
      val searchExceptElse = items.values.map(i => SearchExcept(i.file.hash, Set(i.file.index)))

      //TODO conf max search time 60 seconds and koeff
      val maxSearchTimeSec = Math.min((search.maxSearchTimeSec * 1.3).toInt, 60)
      val newSearch = search.copy(searchExcept = searchExceptOld ++ searchExceptElse, maxSearchTimeSec = maxSearchTimeSec)
      context.become(waitSearchResult(newSearch, m.user))
      doSearch(newSearch, m.user)
    //wait number of selection
    //FIXME java.lang.NumberFormatException: For input string: and no tgg output
    case m: TextMessage if items.contains(m.text.toInt) =>
      val selected = items(m.text.toInt)
      context.become(waitPlayResult(m.user))
      sttp
        .body(Json.toJson(PlayInput(selected.file.hash, selected.index, conf.playOn)).toString())
        .post(uri"${conf.smartRatUrl}/api/v1/play")
        .response(asString)
        .send.map(_.unsafeBody)
        .timeout(conf.playTimeout)
        .redeem(e => FallbackToUser(e, m.user), identity)
        .runToFuture
        .pipeTo(self)
    case m: TextMessage =>
      senders(m.user.interaction).sendText("Не выбрал ты ничего. Ладно", m.user)
      context.become(waitCommand)
  }

  def waitPlayResult(user: User): Receive = toUserErrorHandle orElse {
    case result: String =>
      context.become(waitCommand)
      senders(user.interaction).sendText(result, user)
  }

  override def receive: Receive = waitCommand
}