package org.zella.tgrat.server

import akka.actor.ActorRef
import com.typesafe.scalalogging.LazyLogging
import io.vertx.scala.core.{Vertx, http}
import io.vertx.scala.ext.web.Router
import io.vertx.scala.ext.web.handler.BodyHandler
import monix.eval.Task
import org.zella.tgrat.BuildInfo
import org.zella.tgrat.config.TgRatConfig
import org.zella.tgrat.model.{SpeechUser, TextMessage}
import play.api.libs.json.{Format, Json}
import monix.execution.Scheduler.Implicits.global

case class SttInput(text: String, userId: String)

object SttInput {
  implicit val format: Format[SttInput] = Json.format[SttInput]
}

class HttpEndpoints(bot: ActorRef, conf: TgRatConfig) extends LazyLogging {
  def startT(): Task[http.HttpServer] = {
    Task.fromFuture {
      val vertx = Vertx.vertx()
      val router = Router.router(vertx)
      router.route.handler(BodyHandler.create())
      router.post("/command") handler (ctx => {
        Task {
          ctx.getBodyAsString()
            .map(Json.parse(_).as[SttInput]) match {
            case Some(i) =>
              bot ! TextMessage(i.text, SpeechUser(i.userId))
              ctx.response().end(Json.toJson("ok").toString())
            case None => ctx.fail(400)
          }
        }.onErrorRecover {
          case e => ctx.fail(e)
        }.runAsyncAndForget
      })
      router.get("/about").handler(ctx => ctx.response().end(BuildInfo.version))
      vertx
        .createHttpServer()
        .requestHandler(router.accept _)
        .listenFuture(conf.httpPort, "0.0.0.0")
    }
  }
}
