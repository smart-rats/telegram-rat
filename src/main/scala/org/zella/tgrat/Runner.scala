package org.zella.tgrat

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import monix.eval.Task
import monix.execution.Ack
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import org.zella.tgrat.config.impl.TypesafeConfig
import org.zella.tgrat.core.MediaSearcher
import org.zella.tgrat.messages.MyTelegramBot
import org.zella.tgrat.messages.impl.{GttsMessageSender, TelegramMessageSender}
import org.zella.tgrat.model.{Interaction, SpeechUser, TextMessage}
import org.zella.tgrat.utils.{Subprocess, Utils}

import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, FiniteDuration}

object Runner extends LazyLogging {
  def main(args: Array[String]): Unit = {

    val config = new TypesafeConfig(ConfigFactory.defaultApplication().resolve())

    val bot = new MyTelegramBot(config)

    val senders = Map(Interaction.Telegram -> new TelegramMessageSender(bot)) ++ config.gassistantEmbeddedSpeech
      .map(c => Map(Interaction.Speech -> new GttsMessageSender(c))).getOrElse(Map())

    val system = ActorSystem("rat-system")

    val source = system.actorOf(Props(new MediaSearcher(senders, config)), "source")

    bot.setSource(source)

    config.gassistantEmbeddedSpeech.foreach(config => {
      Utils.retryWithDelay(Observable.fromReactivePublisher(Subprocess.hotword(config.scriptHotWord,
        config.assistantDeviceId,
        config.assistantCredentials)
        .doOnNext(s => logger.debug("Hotword detected"))
        .doOnError(err => logger.error("err", err))), 1.seconds)
        .subscribeOn(global)
        .mapEval(_ => Observable.fromReactivePublisher(Subprocess.recognize(config.scriptRecognize,
          config.speechRecognizeKey).toFlowable).firstL)
        .subscribe(text => {
          logger.debug(text)
          //speaker recognition not supported
          source ! TextMessage(text, SpeechUser("n/a"))
          Ack.Continue
        }, err => logger.error("err", err))

    })

    val eol = bot.run()
    println("Press [ENTER] to shutdown the bot, it may take a few seconds...")
    scala.io.StdIn.readLine()
    bot.shutdown() // initiate shutdown
    // Wait for the bot end-of-life
    Await.result(eol, Duration.Inf)
  }

}
