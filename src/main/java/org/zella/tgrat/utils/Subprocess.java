package org.zella.tgrat.utils;

import com.github.davidmoten.rx2.Strings;
import com.github.zella.rxprocess2.Exit;
import com.github.zella.rxprocess2.RxNuProcessBuilder;
import io.reactivex.*;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Subprocess {

    static {
        //TODO fix default timeout and "no timeout" on rx-process2 library
        //24 days
        System.setProperty("rxprocess2.defaultTimeoutMillis", String.valueOf(Integer.MAX_VALUE));
    }

    public static Flowable<String> hotword(Path script, String deviceId, Path credentials) {
        return RxNuProcessBuilder
                .fromCommand(List.of("python3", script.toAbsolutePath().toString(),
                        "--credentials", credentials.toString(),
                        "--device-model-id", "'" + deviceId + "'"))
                .asStdOut().toFlowable(BackpressureStrategy.BUFFER)
                .compose(src -> Strings.decode(src, Charset.defaultCharset()))
                .compose(src -> Strings.split(src, System.lineSeparator()))
                .filter(s -> s.startsWith("[trigger]"));
    }

    //TODO lang
    public static Single<String> recognize(Path script, String key) {
        return RxNuProcessBuilder
                .fromCommand(List.of("python3", script.toAbsolutePath().toString(), key))
                .asStdOutSingle()
                .map(String::new)
                .map(s -> Arrays.stream(s.split(System.lineSeparator()))
                        .filter(ss -> ss.startsWith("[recognized]"))
                        .map(sss -> sss.replace("[recognized]", "")).collect(Collectors.joining()));

    }

    public static Completable playGtts(Path script, String text, String lang) {
        return RxNuProcessBuilder
                .fromCommand(List.of("python3", script.toAbsolutePath().toString(), text, lang))
                .asWaitDone()
                .ignoreElement();


    }

}
