package org.zella.tgrat.utils;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

public class JUtils {
    /**
     *
     * @param textRaw
     * @return lower case words
     */
    public static List<String> wordsLowerCase(String textRaw) {
        String text = textRaw.toLowerCase().trim();
        List<String> words = new ArrayList<>();
        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(text);
        int lastIndex = breakIterator.first();
        while (BreakIterator.DONE != lastIndex) {
            int firstIndex = lastIndex;
            lastIndex = breakIterator.next();
            if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(text.charAt(firstIndex))) {
                words.add(text.substring(firstIndex, lastIndex));
            }
        }

        return words;
    }
}
