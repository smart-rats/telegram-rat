package org.zella.tgrat.command

import org.scalatest._
import org.zella.tgrat.model.{Any, CommandDef, Interaction, Variant, Word}
import play.api.libs.json.Json

class CommandDefSpec extends FlatSpec with Matchers {
  "CommandDef" should "have valid regexp representation" in {


    //film -> {киноха, фильмас}
    //song -> {песня, трек}

    val cmd = CommandDef(List(
      Word("find", Set(Variant("find", Set("найди", "поищи")))),
      Word("media", Set(Variant("film", Set("кино", "фильм", "мультик")), Variant("song", Set("песню", "композицию", "музыку")))),
      Any("subject")), Set(Interaction.Telegram))

    println(Json.toJson(Seq(cmd)))

    //TODO
    //    val source = CommandDef(List(Word("Сделай"), Word("киношку", Set("фильм")), Any, Word("громче")))
    //
    //    source.matches(Seq("Сделай", "фильм", "Зеленый", "слоник", "2", "громче")) shouldBe true
    //
    //    source.matches(Seq("Сделай", "киношку", "Зеленый", "слоник", "2", "громче")) shouldBe true
    //
    //    source.matches(Seq("Сделай", "бяку", "Зеленый", "слоник", "2", "громче")) shouldBe false
  }
}
